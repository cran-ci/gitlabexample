FROM rocker/r-ver:latest

ARG PKG_INSTALL

RUN apt-get update -y
RUN apt-get install -y apt-utils
RUN apt-get install -y libxml2 xml2
RUN apt-get install -y libxml2-dev
RUN apt-get install -y libcurl4-openssl-dev libssh2-1-dev libssl-dev git
RUN apt-get install -y qpdf pandoc pandoc-citeproc openssh-client curl
RUN apt-get install -y libgit2-dev
RUN R -e 'install.packages(c("roxygen2"), repos="http://cran.rstudio.com", dependencies = TRUE)'
RUN R -e 'install.packages(c("git2r","usethis","devtools","testthat"), repos="http://cran.rstudio.com", dependencies = TRUE)'

RUN R -e "${PKG_INSTALL}"

RUN R -e 'install.packages(c("covr", "DT"), repos="http://cran.rstudio.com", dependencies = TRUE)'
RUN R -e 'install.packages(c("pkgdown"), repos="http://cran.rstudio.com", dependencies = TRUE)'
RUN R -e 'install.packages("rvest", repos = "https://cran.rstudio.com", dependencies = TRUE)'
RUN R -e 'install.packages(c("lintr"), repos="http://cran.rstudio.com", dependencies = TRUE)'

RUN R --version
