FROM rocker/verse:latest

RUN apt-get update -y
RUN apt install -y curl libxt-dev libharfbuzz-dev libfribidi-dev libfreetype6-dev libpng-dev libtiff5-dev libjpeg-dev
RUN apt install -y tree rsync
RUN R -e 'install.packages(c("pkgdown", "covr", "DT", "drat"), repos="http://cran.rstudio.com", dependencies = TRUE)'

RUN R --version